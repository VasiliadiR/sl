﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Letter : MonoBehaviour
{
    public Vector3 shuffleCoordinates;
    public Text letterText;
    public float speed = 0.05F;
    public float time = 0.01f;

    private void Start()
    {
        Random rand = new Random();
        char tmp = (char)Random.Range('А', 'Я' + 1);    // генерация случайной буквы
        letterText.text = tmp.ToString();
        shuffleCoordinates = gameObject.transform.position; // будущие новые координаты
        GenerateField.Generated += Move;    //  активация движения по событию рассчета новых координат
    }
    public void Move()
    {
        StopAllCoroutines();
        StartCoroutine(GoToNewPlace());
    }
    IEnumerator GoToNewPlace()
    {
        while (Vector3.Distance(gameObject.transform.position, shuffleCoordinates) > 0.1f)
        {
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, shuffleCoordinates, speed);    //объекты перемещаются таким образом, чтобы закончить движение одновременно
            yield return new WaitForSeconds(time);
        }    
    }
    private void OnDestroy()
    {
        GenerateField.Generated -= Move;
    }
}
