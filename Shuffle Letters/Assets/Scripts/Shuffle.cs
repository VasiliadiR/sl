﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuffle : MonoBehaviour
{
    public static void GenerateNewPositions(List<GameObject> positions) //скрипт отвечает за перемешивание координат
    {
        for (int i = positions.Count - 1; i >= 1; i--)  // полю shuffleCoordinates присваивается координаты другой случайной буквы
        {
            int j = Random.Range(0, i); 
            Vector3 tmp = positions[j].GetComponent<Letter>().shuffleCoordinates;
            positions[j].GetComponent<Letter>().shuffleCoordinates = positions[i].GetComponent<Letter>().shuffleCoordinates;
            positions[i].GetComponent<Letter>().shuffleCoordinates = tmp;
        }
        
    }
}
