﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using System;
using UnityEngine.UI;

public class GenerateField : MonoBehaviour
{
    public static event Action Generated;

    public Text widthText;
    public Text heightText;

    public GameObject particle;
    public int size = 0;
    public List<GameObject> positionList = new List<GameObject>();
    private static List<GameObject> _newPositionList = new List<GameObject>();

    private readonly float _pixels = 1000;
    private readonly float _minScale = 100;
    private readonly float _indentMax = 6.5f;

    private GameObject _bukvar;

    public void Generate()  // метод активируется по кнопке "Сгенерировать"
    {
        positionList.Clear();
        Destroy(_bukvar);      
        _bukvar = new GameObject();  //родительский GameObject для сгенерированных букв
        _bukvar.name = "Azbuka";
        if (int.TryParse(widthText.text, out int intWidth))
            intWidth = int.Parse(widthText.text);
        if (int.TryParse(heightText.text, out int intHeight))
            intHeight = int.Parse(heightText.text);
        if (intWidth*intHeight != 0)    // создаем букварь, если и ширина, и длина отличны от нуля
            Calculation(intWidth, intHeight);
    }
    public void Calculation(int intWidth, int intHeight)
    {
        int maxDimension = intWidth > intHeight ? intWidth : intHeight; // определяем большую размерность
        int minDimension = intWidth > intHeight ? intHeight : intWidth;

        float maxDimStep = _pixels / maxDimension;      // шаг для большей размерности
        float newScale = maxDimStep - 2 * _indentMax;  // новый масштаб для большей размерности
        float minDimStep = _pixels / (minDimension +1); // шаг для меньшей размерности

        float firstPointMaxDim = (_indentMax + (newScale - _minScale) / 2 + _minScale/2);   //точка отсчета для большей размерности


        float dimX = intWidth > intHeight ? firstPointMaxDim : minDimStep;  // определяем, какие коэффициенты будут передаваться в метод
        float dimZ = intWidth > intHeight ? minDimStep : firstPointMaxDim;

        float kX = intWidth > intHeight ? maxDimStep : minDimStep;
        float kZ = intWidth > intHeight ? minDimStep : maxDimStep;

        CreateLetters(intWidth, intHeight, dimX, dimZ, kX, kZ, newScale);
    }
    public void CreateLetters(int intWidth, int intHeight, float dimX, float dimZ, float kX, float kZ, float newScale)
    {
        for (int i = 0; i < intWidth; i++)
            for (int j = 0; j < intHeight; j++)
            {
                var unit = Instantiate(particle, _bukvar.transform);

                unit.transform.position = new Vector3(dimX + i*kX, 50, dimZ + j*kZ);    //расположение элементов
                unit.transform.localScale = new Vector3(newScale, newScale, newScale);  // определение масштаба
                positionList.Add(unit);
                unit.name = unit.name.Replace("(Clone)", "") + positionList.Count;  // для красоты
            }
    }
    public void ShuffleLetter() // метод активируется по кнопке "Перемешать"
    {
        Shuffle.GenerateNewPositions(positionList);
        Generated?.Invoke();    // по данному событию будет запускаться метод, двигающий буквы
    }
}